import cirq
from cirq import H, X, CZ, measure


def increment_oracle():
    qubits = cirq.LineQubit.range(2)
    circuit = cirq.Circuit()
    circuit.append([cirq.CNOT(qubits[0], qubits[1]), X(qubits[0])])
    return circuit, qubits


# Функция для создания оракула, который проверяет, равно ли x+1 трём
def check():
    qubits = cirq.LineQubit.range(2)
    circuit = cirq.Circuit()
    circuit.append([X(qubits[0]), X(qubits[1])])
    return circuit, qubits


def grovers_algorithm(oracle, qubits):
    circuit = cirq.Circuit()
    circuit.append([H(q) for q in qubits])
    circuit.append(oracle)
    circuit.append([H(q) for q in qubits])
    circuit.append([X(q) for q in qubits])
    circuit.append(CZ(qubits[0], qubits[1]))
    circuit.append([X(q) for q in qubits])
    circuit.append([H(q) for q in qubits])
    circuit.append(measure(*qubits, key='result'))
    return circuit


increment_circuit, increment_qubits = increment_oracle()
check_circuit, check_qubits = check()
grovers_circuit = grovers_algorithm(check_circuit, check_qubits)

simulator = cirq.Simulator()
result = simulator.run(grovers_circuit, repetitions=10)

print(result)
