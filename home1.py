import cirq

circuit = cirq.Circuit()
qubits = cirq.LineQubit.range(4)

circuit.append(cirq.CCX(qubits[0], qubits[1], qubits[3]))
circuit.append(cirq.CCX(qubits[0], qubits[2], qubits[3]))
circuit.append(cirq.CCX(qubits[1], qubits[2], qubits[3]))

circuit.append(cirq.measure(qubits[3], key='result'))
simulator = cirq.Simulator()
resultSimulate = simulator.simulate(circuit)

result = simulator.run(circuit, repetitions=10)
print(resultSimulate)

print(result)