import cirq
import numpy as np


def make_phase_estimation(U, psi, n, Nreps):
    qubits = cirq.LineQubit.range(n)
    u_bit = cirq.NamedQubit('u')

    phase_estimator = cirq.Circuit()
    phase_estimator.append(psi(u_bit))
    phase_estimator.append(cirq.H.on_each(*qubits))
    for i, bit in enumerate(qubits):
        phase_estimator.append(cirq.ControlledGate(U).on(bit, u_bit) ** (2 ** (n - i - 1)))
    phase_estimator.append(cirq.qft(*qubits[::-1], without_reverse=True, inverse=True))
    phase_estimator.append(cirq.measure(*qubits, key='theta'))

    sim = cirq.Simulator()
    results = sim.run(phase_estimator, repetitions=Nreps)

    theta = np.sum(2 ** np.arange(n) * results.measurements['theta'], axis=1) / 2 ** n
    return theta


U = cirq.Z ** (1 / 3)
psi = lambda q: cirq.X(q)
n = 4
Nreps = 10

theta_estimate = make_phase_estimation(U, psi, n, Nreps)
print(theta_estimate)
