import cirq


def inverse_qft(circuit, qubits):
    n = len(qubits)
    for j in range(n // 2):
        circuit.append(cirq.SWAP(qubits[j], qubits[n - j - 1]))

    for i in range(n):
        for j in range(i):
            circuit.append(cirq.CZ(qubits[j], qubits[i]) ** (-1 / 2 ** (i - j)))
        circuit.append(cirq.H(qubits[i]))


def create_iqft_circuit(n):
    qubits = [cirq.LineQubit(i) for i in range(n)]
    circuit = cirq.Circuit()
    inverse_qft(circuit, qubits)
    return circuit
