import fractions


def find_period(y, Q):
    # Нахождение периода с помощью непрерывных дробей
    frac = fractions.Fraction(y, Q).limit_denominator(Q)
    return frac.denominator


def gcd(a, b):
    while b:
        a, b = b, a % b
    return a


def factorize(n, Q, a, y):
    r = find_period(y, Q)

    if r % 2 != 0 or pow(a, r // 2, n) == n - 1:
        return "Необходимо повторить процедуру с другим a"

    p = gcd(pow(a, r // 2) - 1, n)
    q = gcd(pow(a, r // 2) + 1, n)

    if p == n or q == n:
        return "Необходимо повторить процедуру с другим a"

    return p, q


result = factorize(15, 1024, 7, 512)
print(result)
